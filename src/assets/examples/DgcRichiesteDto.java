package it.dacinformatica.efwm.v1.models.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.dacinformatica.efwm.v1.models.entity.DgcRichiesteTrasporto;
import it.dacinformatica.efwm.v1.models.entity.Impianti;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DgcRichiesteDto {
	private BigDecimal idDgcRichieste;
	private Date dataRichiesta;
	private Date dataConsegnaRichiesta;
	private String edrcDestina;
	private String erdcCedente;
	private BigDecimal idUtenteDestina;
	private BigDecimal nrRichiesta;
	private List<DgcRichiesteDettaglioDto> dgcRichiesteDettaglios;
	private List<DgcRichiesteStatoDto> dgcRichiesteStatos;
	private List<DgcRichiesteTrasportoDto> dgcRichiesteTrasportos;
	
	private ImpiantiDto impiantoCedente;
	private ImpiantiDto impiantoDestinatario;
	
	// questi li invio nella richiesta di salvataggio
	private String indirizzo;
	private String citta;
	private String cap;
	private String provincia;
	private String caserma;
	private String dislAmministrativa;
	
	//
	private BigDecimal qta;
	private String ultimoStato;
	private String ultimaNota;
	
	private String denominazioneCarburante;
	private boolean trasmissione;
	
	private BigDecimal idUtente;
	private String note;

	private BigDecimal qtaCalcolata;

	public DgcRichiesteDto(BigDecimal idDgcRichieste, BigDecimal qtaCalcolata) {
		super();
		this.idDgcRichieste = idDgcRichieste;
		this.qtaCalcolata = qtaCalcolata;
	}
	
	
}
