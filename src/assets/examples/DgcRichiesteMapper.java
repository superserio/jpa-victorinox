package it.dacinformatica.efwm.v1.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import it.dacinformatica.efwm.v1.models.dto.DgcRichiesteDto;
import it.dacinformatica.efwm.v1.models.entity.DgcRichieste;

@Mapper(config = BaseMapper.class, uses = {})
public interface DgcRichiesteMapper {
	DgcRichiesteMapper INSTANCE = Mappers.getMapper(DgcRichiesteMapper.class);
	
	@Mappings({ //NOSONAR
				@Mapping(target="idDgcRichieste",				source="idDgcRichieste")
			, 	@Mapping(target="dataRichiesta", 				source="dataRichiesta")
			, 	@Mapping(target="dataConsegnaRichiesta", 		source="dataConsegnaRichiesta")
			, 	@Mapping(target="edrcDestina", 					source="edrcDestina")
			, 	@Mapping(target="erdcCedente", 					source="erdcCedente")
			, 	@Mapping(target="idUtenteDestina", 				source="idUtenteDestina")
			, 	@Mapping(target="nrRichiesta", 					source="nrRichiesta")
			, 	@Mapping(target="dgcRichiesteDettaglios", 		source="dgcRichiesteDettaglios")
			, 	@Mapping(target="dgcRichiesteStatos", 			source="dgcRichiesteStatos")
			, 	@Mapping(target="dgcRichiesteTrasportos", 		source="dgcRichiesteTrasportos")
			, 	@Mapping(target="impiantoCedente", 				source="impiantoCedente")
			, 	@Mapping(target="impiantoDestinatario", 		source="impiantoDestinatario")
	})
	DgcRichieste dgcRichiesteDtoToDgcRichieste(DgcRichiesteDto dto);

	DgcRichiesteDto dgcRichiesteToDgcRichiesteDto(DgcRichieste entity);
	List<DgcRichiesteDto> dgcRichiesteListToDgcRichiesteDtoList(List<DgcRichieste> entityList);
	List<DgcRichieste> dgcRichiesteDtoListToDgcRichiesteList(List<DgcRichiesteDto> dtoList);
}
