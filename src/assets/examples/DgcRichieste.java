package it.dacinformatica.efwm.v1.models.entity;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the DGC_RICHIESTE database table.
 * 
 */
@Entity
@Table(name="DGC_RICHIESTE")
@NamedQuery(name="DgcRichieste.findAll", query="SELECT d FROM DgcRichieste d")
public class DgcRichieste implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3982695597133998333L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_DCG_RICHIESTE")
    @SequenceGenerator(name = "SEQ_DCG_RICHIESTE", sequenceName = "SEQ_DCG_RICHIESTE", allocationSize=1)
	@Column(name = "ID", unique = true, nullable = false, precision = 30, scale = 0)
	private BigDecimal idDgcRichieste;

//	@Temporal(TemporalType.DATE)
	@Column(name="DATA_RICHIESTA")
	private Date dataRichiesta;
	
//	@Temporal(TemporalType.DATE)
	@Column(name="DATA_CONSEGNA_RICHIESTA")
	private Date dataConsegnaRichiesta;

	@Column(name="EDRC_DESTINA")
	private String edrcDestina;

	@Column(name="ERDC_CEDENTE")
	private String erdcCedente;

	@Column(name="ID_UTENTE_DESTINA")
	private BigDecimal idUtenteDestina;

	@Column(name="NR_RICHIESTA")
	private BigDecimal nrRichiesta;

	//bi-directional many-to-one association to DgcRichiesteDettaglio
	@OneToMany(mappedBy="dgcRichiesteDettaglio")
	private List<DgcRichiesteDettaglio> dgcRichiesteDettaglios;

	//bi-directional many-to-one association to DgcRichiesteStato
	@OneToMany(mappedBy="dgcRichiesteStato")
	private List<DgcRichiesteStato> dgcRichiesteStatos;
	
	//bi-directional many-to-one association to DgcRichiesteTrasporto
	@OneToMany(mappedBy="dgcRichiesteTrasporto")
	private List<DgcRichiesteTrasporto> dgcRichiesteTrasportos;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_IMPIANTO_CEDENTE")
	private Impianti impiantoCedente;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_IMPIANTO_DEST")
	private Impianti impiantoDestinatario;

	public Impianti getImpiantoCedente() {
		return impiantoCedente;
	}

	public void setImpiantoCedente(Impianti impiantoCedente) {
		this.impiantoCedente = impiantoCedente;
	}

	public Impianti getImpiantoDestinatario() {
		return impiantoDestinatario;
	}

	public void setImpiantoDestinatario(Impianti impiantoDestinatario) {
		this.impiantoDestinatario = impiantoDestinatario;
	}

	public DgcRichieste() {
	}

	public BigDecimal getIdDgcRichieste() {
		return this.idDgcRichieste;
	}

	public void setIdDgcRichieste(BigDecimal idDgcRichieste) {
		this.idDgcRichieste = idDgcRichieste;
	}

	public Date getDataRichiesta() {
		return this.dataRichiesta;
	}

	public void setDataRichiesta(Date dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}
	
	public Date getDataConsegnaRichiesta() {
		return dataConsegnaRichiesta;
	}

	public void setDataConsegnaRichiesta(Date dataConsegnaRichiesta) {
		this.dataConsegnaRichiesta = dataConsegnaRichiesta;
	}

	public String getEdrcDestina() {
		return this.edrcDestina;
	}

	public void setEdrcDestina(String edrcDestina) {
		this.edrcDestina = edrcDestina;
	}

	public String getErdcCedente() {
		return this.erdcCedente;
	}

	public void setErdcCedente(String erdcCedente) {
		this.erdcCedente = erdcCedente;
	}

	public BigDecimal getIdUtenteDestina() {
		return this.idUtenteDestina;
	}

	public void setIdUtenteDestina(BigDecimal idUtenteDestina) {
		this.idUtenteDestina = idUtenteDestina;
	}

	public BigDecimal getNrRichiesta() {
		return this.nrRichiesta;
	}

	public void setNrRichiesta(BigDecimal nrRichiesta) {
		this.nrRichiesta = nrRichiesta;
	}


	public List<DgcRichiesteDettaglio> getDgcRichiesteDettaglios() {
		return this.dgcRichiesteDettaglios;
	}

	public void setDgcRichiesteDettaglios(List<DgcRichiesteDettaglio> dgcRichiesteDettaglios) {
		this.dgcRichiesteDettaglios = dgcRichiesteDettaglios;
	}

	public DgcRichiesteDettaglio addDgcRichiesteDettaglio(DgcRichiesteDettaglio dgcRichiesteDettaglio) {
		getDgcRichiesteDettaglios().add(dgcRichiesteDettaglio);
		dgcRichiesteDettaglio.setDgcRichieste(this);

		return dgcRichiesteDettaglio;
	}

	public DgcRichiesteDettaglio removeDgcRichiesteDettaglio(DgcRichiesteDettaglio dgcRichiesteDettaglio) {
		getDgcRichiesteDettaglios().remove(dgcRichiesteDettaglio);
		dgcRichiesteDettaglio.setDgcRichieste(null);

		return dgcRichiesteDettaglio;
	}


	public List<DgcRichiesteStato> getDgcRichiesteStatos() {
		return this.dgcRichiesteStatos;
	}

	public void setDgcRichiesteStatos(List<DgcRichiesteStato> dgcRichiesteStatos) {
		this.dgcRichiesteStatos = dgcRichiesteStatos;
	}

	public DgcRichiesteStato addDgcRichiesteStato(DgcRichiesteStato dgcRichiesteStato) {
		getDgcRichiesteStatos().add(dgcRichiesteStato);
		dgcRichiesteStato.setDgcRichieste(this);

		return dgcRichiesteStato;
	}

	public DgcRichiesteStato removeDgcRichiesteStato(DgcRichiesteStato dgcRichiesteStato) {
		getDgcRichiesteStatos().remove(dgcRichiesteStato);
		dgcRichiesteStato.setDgcRichieste(null);

		return dgcRichiesteStato;
	}
	
	public List<DgcRichiesteTrasporto> getDgcRichiesteTrasportos() {
		return dgcRichiesteTrasportos;
	}

	public void setDgcRichiesteTrasportos(List<DgcRichiesteTrasporto> dgcRichiesteTrasportos) {
		this.dgcRichiesteTrasportos = dgcRichiesteTrasportos;
	}
	
	public DgcRichiesteTrasporto addDgcRichiesteTrasporto(DgcRichiesteTrasporto dgcRichiesteTrasporto) {
		getDgcRichiesteTrasportos().add(dgcRichiesteTrasporto);
		dgcRichiesteTrasporto.setDgcRichieste(this);

		return dgcRichiesteTrasporto;
	}

	public DgcRichiesteTrasporto removeDgcRichiesteTrasporto(DgcRichiesteTrasporto dgcRichiesteTrasporto) {
		getDgcRichiesteTrasportos().remove(dgcRichiesteTrasporto);
		dgcRichiesteTrasporto.setDgcRichieste(null);

		return dgcRichiesteTrasporto;
	}

}