export const names = new Map([
    ["Entity", "@Entity\n@Table(name=\"MY_TABLE\")\npublic class MyTable implements serializable {\n\t// ...\n}"]
  , ["DTO", "@Getter\n@Setter\n@NoArgsConstructor\n@AllArgsConstructor\npublic class MyTable {\n\t// ...\n}"]
  , ["Mapper", "@Mapper(config = BaseMapper.class, uses = { })\npublic interface MyTableMapper {\n\t// ...\n}"]
]);

export const templateDto = `@Getter\n@Setter\n@NoArgsConstructor\n@AllArgsConstructor`;

export const templateMapper = `import org.mapstruct.Mapper;\nimport org.mapstruct.Mapping;\nimport org.mapstruct.Mappings;\nimport org.mapstruct.factory.Mappers;\n@Mapper(config = BaseMapper.class, uses = { })`;

export const dataTypes =  [  
                            "BigDecimal", "double", "String", "int", "Long", "long", "Integer", "char", "Date",
                            "List<BigDecimal>", "List<double>", "List<String>", "List<int>", "List<Long>", "List<long>", "List<Integer>", "List<char>", "List<Date>"
                          ];