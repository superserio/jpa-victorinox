import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GestioneTextareaComponent } from './gestione-textarea/gestione-textarea.component';
import { TextareaComponent } from './textarea/textarea.component';
import { TextareaDtoComponent } from './textarea-dto/textarea-dto.component';
import { TextareaMapperComponent } from './textarea-mapper/textarea-mapper.component';

@NgModule({
  declarations: [
    AppComponent,
    GestioneTextareaComponent,
    TextareaComponent,
    TextareaDtoComponent,
    TextareaMapperComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
