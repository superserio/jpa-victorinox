import { Component, OnInit, Input } from '@angular/core';
import { TextChangeService } from '../shared/text-change.service';
import { names, templateMapper, dataTypes } from '../const';

@Component({
  selector: 'app-textarea-mapper',
  templateUrl: './textarea-mapper.component.html',
  styleUrls: ['./textarea-mapper.component.css']
})
export class TextareaMapperComponent implements OnInit {
  formattedOutput: string;
  placeholder: string;

  constructor(private textChangeService : TextChangeService) { }

  ngOnInit(): void {
    this.placeholder = names.get("Mapper");

    this.textChangeService.onDataChange(txt => {
      if(txt !== "") {
        let arrayOfLines = txt.match(/[^\r\n]+/g);
        let className = "", vars = [], instanceDecl = "", fullClassName = "";
  
        arrayOfLines.forEach((element) => {
          if(element.includes("public class")) {
            className = `public interface ${element.split(" ")[2]}Mapper {`;
            fullClassName = element.split(" ")[2];
            instanceDecl = `\t${element.split(" ")[2]}Mapper INSTANCE = Mappers.getMapper(${element.split(" ")[2]}Mapper.class);`;
          }
          if(element.includes("private") && element.split(" ")[1] != "static") {
            let varName = element.split(" ")[2].split("");
            varName.pop();
            varName = varName.join("");
            vars.push(`\t\t@Mapping(target="${varName}",\t\t\t\tsource="${varName}"),\n`);
          }
        });

        // Remove comma last mapping
        let lastLine = vars[vars.length - 1].slice(0, -2);
        vars.pop();
        vars.push(lastLine);
      
        let dtoToEntity = `${fullClassName} dtoToEntity(${fullClassName}Dto dto)`;
        let entityToDto = `${fullClassName}Dto entityToDto(${fullClassName} entity)`;
        let entityListToDtoList = `List<${fullClassName}Dto> entityListToDtoList(List<${fullClassName}> entityList)`;
        let entityDtoListToList = `List<${fullClassName}> entityDtoListToList(List<${fullClassName}Dto> dtoList)`;
        
        this.formattedOutput = `${templateMapper}\n${className}\n${instanceDecl}\n\t@Mappings({ //NOSONAR\n${vars.join("")}\n\t})\n\t${dtoToEntity}\n\t${entityToDto}\n\t${entityListToDtoList}\n\t${entityDtoListToList}\n}`;
      } else this.formattedOutput = "";
    }) 
  } 

}
