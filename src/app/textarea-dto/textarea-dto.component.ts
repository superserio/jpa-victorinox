import { Component, OnInit, Input } from '@angular/core';
import { TextChangeService } from '../shared/text-change.service';
import { names, templateDto, dataTypes } from '../const';

@Component({
  selector: 'app-textarea-dto',
  templateUrl: './textarea-dto.component.html',
  styleUrls: ['./textarea-dto.component.css']
})
export class TextareaDtoComponent implements OnInit {
  formattedOutput: string;
  placeholder: string;

  constructor(private textChangeService : TextChangeService) { }

  ngOnInit(): void {
    this.placeholder = names.get("DTO");

    this.textChangeService.onDataChange(txt => {
      if(txt !== "") {
        let arrayOfLines = txt.match(/[^\r\n]+/g);
        let className = "", vars = [];
  
        arrayOfLines.forEach(element => {
          if(element.includes("public class")) className = `public class ${element.split(" ")[2]}Dto {\n`;
          if(element.includes("private") && element.split(" ")[1] != "static") {
            if(dataTypes.includes(element.split(" ")[1])) vars.push(element+"\n");
            else {
              // Parsing non primitive dataTypes
              if(!element.split(" ")[1].includes("List")) {
                element.split(" ")[1] = element.split(" ")[1]+"Dto";
                vars.push(`\tprivate ${element.split(" ")[1]}Dto ${element.split(" ")[2]}\n`);
              } else {
                // Parsing List<CLASS> for non primitive data types    
                let indexSimboloMagg = element.split(" ")[1].indexOf(">");
                let result = element.split(" ")[1].substring(5, indexSimboloMagg);
                vars.push(`\tprivate List<${result}Dto> ${element.split(" ")[2]}\n`);
              }
              
            }
          }
        });
  
        
        this.formattedOutput = `${templateDto}\n${className}${vars.join("")}\n}`;
      } else this.formattedOutput = "";
    }) 
  } 

}
