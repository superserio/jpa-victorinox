import { Input, Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { TextChangeService } from '../shared/text-change.service';
import { names } from '../const';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css']
})

export class TextareaComponent implements OnInit {
  @Output() 
  textChange = new EventEmitter<string>();

  placeholder: string;

  constructor(private textChangeService : TextChangeService) { }

  ngOnInit(): void {
    this.placeholder = names.get("Entity");
  }

  valueChange(text) {
    this.textChangeService.setData = text;
  }
}
