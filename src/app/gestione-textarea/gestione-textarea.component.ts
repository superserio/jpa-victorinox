import { Component, OnInit } from '@angular/core';
import { TextChangeService } from '../shared/text-change.service';
import { names } from '../const';

@Component({
  selector: 'app-gestione-textarea',
  templateUrl: './gestione-textarea.component.html',
  styleUrls: ['./gestione-textarea.component.css']
})
export class GestioneTextareaComponent implements OnInit {
  currText: string;

  constructor() { }

  ngOnInit(): void {
  }

  updateText(text: string) {
    this.currText = text;
  }

}
