import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GestioneTextareaComponent } from './gestione-textarea/gestione-textarea.component';

const routes: Routes = [
  { path: '', component: GestioneTextareaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
